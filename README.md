# liquidity_analysis

Liquidity analysis of the Russian stock market in 2020. 
Most of the metrics originate from this economics article: [https://www.imf.org/external/pubs/ft/wp/2002/wp02232.pdf]((paper))

The dataset in non-converted form can be found here: http://erinrv.qscalp.ru/. After conversion, the stock data for 6 months may take ~350Gb.
The util used for conversion can be found here: https://www.qscalp.ru/download. Unfortunately, it is an .exe file.